package config

import (
	"rooms/version"

	"github.com/spf13/viper"
)

// Config is a global application configuration
type Config struct {
	DbPath         string
	ElemPerPage    int
	Build          string
	Version        string
	Debug          bool
	RecoverEnabled bool
	Port           int
	Host           string
	LogLevel       string
}

const (
	DefaultDbPath         = "/tmp/rooms.db"
	DefaultPerPage        = 100
	DefaultRecoverEnabled = true
	DefaultDebug          = false
	DefaultLogLevel       = "debug"
	DefaultPort           = 8080
	DefaultHost           = "127.0.0.1"

	KeyElemPerPage    = "elements-per-page"
	KeyDbPath         = "database-path"
	KeyRecoverEnabled = "recover-enabled"
	KeyDebug          = "debug"
	KeyLogLevel       = "logging-level"
	KeyPort           = "port"
	KeyHost           = "host"
)

func ConstructConfig() *Config {
	return &Config{
		DbPath:      viper.GetString(KeyDbPath),
		ElemPerPage: viper.GetInt(KeyElemPerPage),
		Debug:       viper.GetBool(KeyDebug),
		Port:        viper.GetInt(KeyPort),
		Host:        viper.GetString(KeyHost),
		LogLevel:    viper.GetString(KeyLogLevel),
		Version:     version.Version,
	}
}
