package context

import (
	"net/http"
	"rooms/utils/config"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

// AppContext is custom context which is to be used
// instead of the default echo.Context.
type AppContext struct {
	echo.Context
	Config *config.Config
	Db     *gorm.DB
}

func (c *AppContext) Ok(response interface{}) error {
	return c.JSON(http.StatusOK, response)
}

func (c *AppContext) BadRequest(response interface{}) error {
	return c.JSON(http.StatusBadRequest, response)
}

func (c *AppContext) NotFound(response interface{}) error {
	return c.JSON(http.StatusNotFound, response)
}

func (c *AppContext) Conflict(response interface{}) error {
	return c.JSON(http.StatusConflict, response)
}

func (c *AppContext) InternalServerError(response interface{}) error {
	return c.JSON(http.StatusInternalServerError, response)
}

func GetAppContext(c echo.Context) *AppContext {
	return c.(*AppContext)
}

// CustomContextMiddleware is used to inject the AppContext
// To the handler functions.
func CustomContextMiddleware(db *gorm.DB, conf *config.Config) echo.MiddlewareFunc {
	return func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			appCtx := &AppContext{
				Context: c,
				Db:      db,
				Config:  conf,
			}
			return h(appCtx)
		}
	}
}
