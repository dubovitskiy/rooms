// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"rooms/utils/config"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
)

const (
	// configFilename is a name of the config file
	configFilename = "config"

	// configLocationGlobal is a system-wide config location
	configLocationGlobal = "/etc/rooms/"

	// configLocationLocal is user-defined config location
	configLocationLocal = "$HOME/.rooms"

	// configLocationDevel is a development-friendly config location
	configLocationDevel = "."
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "rooms",
	Short: "Meeting rooms application.",
	Long:  `Application that allows you to book meeting rooms.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	flags := rootCmd.PersistentFlags()
	flags.StringVar(&cfgFile, "config", "", "config file (default is $HOME/rooms/rooms.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(configLocationGlobal)
		viper.AddConfigPath(configLocationLocal)
		viper.AddConfigPath(configLocationDevel)

		viper.SetConfigName(configFilename)

		viper.SetDefault(config.KeyDbPath, config.DefaultDbPath)
		viper.SetDefault(config.KeyElemPerPage, config.DefaultPerPage)
		viper.SetDefault(config.KeyRecoverEnabled, config.DefaultRecoverEnabled)
		viper.SetDefault(config.KeyDebug, config.DefaultDebug)
		viper.SetDefault(config.KeyLogLevel, config.DefaultLogLevel)
		viper.SetDefault(config.KeyPort, config.DefaultPort)
		viper.SetDefault(config.KeyHost, config.DefaultHost)

	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
