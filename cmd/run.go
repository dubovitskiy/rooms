package cmd

import (
	"github.com/spf13/viper"

	"rooms/server"
	"rooms/utils/config"

	"github.com/spf13/cobra"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		conf := config.ConstructConfig()
		srv := server.Server(conf)
		server.RunServer(srv, conf.Host, conf.Port)
	},
}

var (
	dbPath         string
	elemPerPage    int
	recoverEnabled bool
	debug          bool
	port           int
	logLevel       string
	host           string
)

func init() {
	rootCmd.AddCommand(runCmd)

	flags := runCmd.PersistentFlags()

	flags.StringVarP(&dbPath, config.KeyDbPath, "e", config.DefaultDbPath, "Path to your sqlite file")
	viper.BindPFlag(config.KeyDbPath, flags.Lookup(config.KeyDbPath))

	flags.IntVarP(&port, config.KeyPort, "p", config.DefaultPort, config.KeyPort)
	viper.BindPFlag(config.KeyPort, flags.Lookup(config.KeyPort))

	flags.IntVarP(&elemPerPage, config.KeyElemPerPage, "l", config.DefaultPerPage, "Elements ro display per page")
	viper.BindPFlag(config.KeyElemPerPage, flags.Lookup(config.KeyElemPerPage))

	flags.BoolVarP(&recoverEnabled, config.KeyRecoverEnabled, "r", config.DefaultRecoverEnabled, "Enable recover middleware")
	viper.BindPFlag(config.KeyRecoverEnabled, flags.Lookup(config.KeyRecoverEnabled))

	flags.BoolVarP(&debug, config.KeyDebug, "d", config.DefaultDebug, "Enable debug")
	viper.BindPFlag(config.KeyDebug, flags.Lookup(config.KeyDebug))

	flags.StringVarP(&logLevel, config.KeyLogLevel, "b", config.DefaultLogLevel, "Log level")
	viper.BindPFlag(config.KeyLogLevel, flags.Lookup(config.KeyLogLevel))

	flags.StringVarP(&host, config.KeyHost, "n", config.DefaultHost, "Host")
	viper.BindPFlag(config.KeyHost, flags.Lookup(config.KeyHost))
}
