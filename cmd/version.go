// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"rooms/version"

	"github.com/spf13/cobra"
)

var asJSON bool
var pretty bool

type jsonReponse struct {
	AppVersion string `json:"version"`
}

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Version",
	Long:  `Version`,
	Run: func(cmd *cobra.Command, args []string) {
		if !asJSON {
			fmt.Println(version.Version)
		} else {
			versionRes := &jsonReponse{
				AppVersion: version.Version,
			}
			var res []byte
			if pretty {
				res, _ = json.MarshalIndent(versionRes, "", "  ")
			} else {
				res, _ = json.Marshal(versionRes)
			}
			fmt.Println(string(res))
		}
		os.Exit(0)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)

	flags := versionCmd.PersistentFlags()
	flags.BoolVarP(&asJSON, "as-json", "j", false, "As JSON")
	flags.BoolVarP(&pretty, "pretty", "p", false, "Pretty-print JSON")
}
