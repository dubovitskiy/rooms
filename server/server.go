package server

import (
	"fmt"

	"rooms/utils/config"
	"rooms/utils/context"
	"rooms/webapp/api/rooms"
	"rooms/webapp/api/users"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
)

func getLoggerLevel(levelName string) log.Lvl {
	levels := make(map[string]log.Lvl)
	levels["debug"] = log.DEBUG
	levels["info"] = log.INFO
	levels["warn"] = log.WARN
	levels["off"] = log.OFF
	levels["error"] = log.ERROR
	level, err := levels[levelName]
	if !err {
		level = levels["debug"]
		msg := fmt.Sprintf("Invalid logger name specified: %s. Using default log level", levelName)
		fmt.Println(msg)
	}
	return level
}

func Server(conf *config.Config) *echo.Echo {
	db, err := gorm.Open("sqlite3", conf.DbPath)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	db.AutoMigrate(&rooms.Room{}, &users.User{})

	e := echo.New()
	e.HideBanner = true
	e.Use(context.CustomContextMiddleware(db, conf))
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Logger())
	if conf.RecoverEnabled {
		e.Use(middleware.Recover())
	}
	e.Debug = conf.Debug

	logLevel := getLoggerLevel(conf.LogLevel)
	e.Logger.SetLevel(logLevel)

	roomsResource := e.Group("/rooms")
	roomsResource.GET("/:roomId", rooms.Get)
	roomsResource.GET("", rooms.List)
	roomsResource.POST("", rooms.Create)
	roomsResource.DELETE(":roomId", rooms.Delete)

	usersResource := e.Group("/users")
	usersResource.POST("", users.Create)
	return e
}

func RunServer(server *echo.Echo, host string, port int) {
	portConf := fmt.Sprintf("%s:%d", host, port)
	server.Logger.Fatal(server.Start(portConf))
}
