package rooms

import (
	"rooms/webapp/common/db"
)

type Room struct {
	db.BaseModel
	Name     string `json:"name" gorm:"unique"`
	Location string `json:"location"`
}
