package rooms

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"rooms/utils/context"
	"rooms/webapp/common/web"
	"rooms/webapp/helpers"

	"github.com/labstack/echo"
)

type listResponse struct {
	Rooms []Room `json:"rooms"`
	Total uint   `json:"total"`
}

type detailResponse struct {
	Room Room `json:"room"`
}

func Create(c echo.Context) error {
	ctx := context.GetAppContext(c)
	logger := c.Logger()

	room := new(Room)
	if err := c.Bind(room); err != nil {
		res := web.NewError("Bad Request")
		return ctx.BadRequest(res)
	}

	// TODO: validate

	if dbc := ctx.Db.Create(room); dbc.Error != nil {
		errMsg := dbc.Error.Error()
		if strings.Contains(errMsg, "UNIQUE") {
			msg := fmt.Sprintf("Room '%s' is already exists", room.Name)
			res := web.NewError(msg)
			return ctx.Conflict(res)
		}
		logger.Error(errMsg)
		res := web.NewError("Internal Error")
		return ctx.InternalServerError(res)
	}
	return ctx.Ok(room)
}

func Get(c echo.Context) error {
	ctx := context.GetAppContext(c)
	logger := c.Logger()

	roomID, err := helpers.GetIntParam(c, "roomId")
	if err != nil {
		logger.Debug(err.Error())
		res := web.NewError("Not Found")
		return ctx.NotFound(res)
	}
	var room Room
	result := ctx.Db.Where("id = ?", roomID).Find(&room)
	if result.Error != nil {
		logger.Debug(result.Error.Error())
		res := web.NewError("Not Found")
		return ctx.NotFound(res)
	}
	res := &detailResponse{Room: room}
	return ctx.Ok(res)
}

func List(c echo.Context) error {
	ctx := context.GetAppContext(c)

	var roomsTotal uint
	var roomsList []Room

	limit, offset := helpers.PagingParams(c, ctx.Config.ElemPerPage)
	ctx.Db.Offset(offset).Limit(limit).Find(&roomsList)
	ctx.Db.Model(&Room{}).Count(&roomsTotal)
	res := &listResponse{
		Rooms: roomsList,
		Total: roomsTotal,
	}
	return ctx.Ok(res)
}

func Delete(c echo.Context) error {
	ctx := c.(*context.AppContext)
	strRoomID := c.Param("strRoomId")
	roomID, err := strconv.Atoi(strRoomID)
	if err != nil {
		msg := fmt.Sprintf("Room with id %d does not exist", roomID)
		res := web.NewError(msg)
		return ctx.NotFound(res)
	}
	return c.String(http.StatusOK, strRoomID)
}
