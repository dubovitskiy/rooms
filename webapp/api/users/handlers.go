package users

import (
	"errors"
	"fmt"
	"rooms/utils/context"
	"rooms/webapp/common/web"
	"strings"

	"github.com/labstack/echo"
)

type userRequest struct {
	Username        string `json:"username"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}

func (r *userRequest) checkPasswords() error {
	if r.Password != r.PasswordConfirm {
		return errors.New("passwords don't match")
	}
	return nil
}

func Create(c echo.Context) error {
	ctx := context.GetAppContext(c)
	logger := c.Logger()

	requestData := new(userRequest)

	if err := c.Bind(requestData); err != nil {
		res := web.NewError("Bad Request")
		return ctx.BadRequest(res)
	}

	if err := requestData.checkPasswords(); err != nil {
		res := web.NewError(err.Error())
		return ctx.BadRequest(res)
	}

	user := &User{Username: requestData.Username}
	user.SetPassword(requestData.Password)

	// Returns a copy of the DB
	if dbc := ctx.Db.Create(user); dbc.Error != nil {
		errMsg := dbc.Error.Error()
		if strings.Contains(errMsg, "UNIQUE") {
			msg := fmt.Sprintf("Room '%s' is already exists", user.Username)
			res := web.NewError(msg)
			return ctx.Conflict(res)
		}
		logger.Error(errMsg)
		res := web.ErrorResponse{Message: "Internal Error"}
		return ctx.InternalServerError(res)
	}
	logMsg := fmt.Sprintf("New user %s created", user.Username)
	logger.Info(logMsg)
	return ctx.Ok(user)
}
