package users

import (
	"rooms/webapp/common/db"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	db.BaseModel
	Username     string `json:"username" gorm:"unique"`
	PasswordHash string `json:"-"`
}

func (u *User) SetPassword(plaintext string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(plaintext), 14)
	if err != nil {
		return err
	}
	u.PasswordHash = string(bytes)
	return nil
}

func (u *User) CheckPassword(plaintext string) error {
	hash := []byte(u.PasswordHash)
	pass := []byte(plaintext)
	err := bcrypt.CompareHashAndPassword(hash, pass)
	if err != nil {
		return err
	}
	return nil
}
