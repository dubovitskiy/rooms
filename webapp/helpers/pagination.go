package helpers

import (
	"github.com/labstack/echo"
	"strconv"
)


func getPositiveIntParam(c echo.Context, name string, fallback int) int {
	var paramValue int
	queryParam := c.QueryParam(name)
	if queryParam == "" {
		paramValue = fallback
	} else {
		value, err := strconv.Atoi(queryParam)
		if err != nil {
			paramValue = fallback
		}
		if value < 1 {
			paramValue = fallback
		} else {
			paramValue = value
		}
	}

	return paramValue
}

func GetPage(c echo.Context) int {
	return getPositiveIntParam(c, "page", 1)
}

func GetLimit(c echo.Context, fallback int) int {
	perPage := getPositiveIntParam(c, "per_page", fallback)
	if perPage > fallback {
		perPage = fallback
	}
	return perPage
}

func ComputeOffset(page, limit int) int {
	return page * limit - limit
}

func PagingParams(c echo.Context, defaultLimit int) (int, int) {
	page := GetPage(c)
	limit := GetLimit(c, defaultLimit)
	return limit, ComputeOffset(page, limit)
}