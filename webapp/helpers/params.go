package helpers

import (
	"github.com/labstack/echo"
	"errors"
	"strconv"
)

func GetIntParam(c echo.Context, name string) (int, error) {
	param := c.Param(name)
	if param == "" {
		return 0, errors.New("param is empty")
	}
	value, err := strconv.Atoi(param)
	if err != nil {
		return 0, errors.New("param is not an integer")
	}
	return value, nil
}
