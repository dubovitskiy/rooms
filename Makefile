DIST = ./bin
BINARY_NAME = rooms
VERSION=$(shell cat ./build)

run:
	@go run main.go run

binary:
	@rm -rf $(DIST)
	@mkdir -p $(DIST)
	@go build \
		-o $(DIST)/$(BINARY_NAME) \
		-ldflags "-X rooms/version.Version=$(VERSION)" \
		main.go

	@# Magic with build increasing
	@seq $$(expr $(VERSION) + 1) $$(expr $(VERSION) + 1) > ./build
